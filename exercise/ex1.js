let http = require("http");

http.createServer((request, response ) => { if(request.url == '/welcome'){
	response.writeHead(200,{'Content-Type': 'text/plain'})
	response.end('Welcome to the world of Node.js')
}
	else if(request.url == '/register'){
	response.writeHead(503, {'Content-Type': 'text/plain'})
	response.end('Page is under maintenance')
}
	else{
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('404\n Page not Found')
	}
}).listen(4001);
console.log(`The server is succesfully running.`)
db.fruits.aggregate([
	 {$unwind : "$origin"},
         {$match:{onSale:true, origin: "Philippines"}},
         {$group:{_id: "Philippines", maxPrice: {$max: "$price"}}}
	 ])